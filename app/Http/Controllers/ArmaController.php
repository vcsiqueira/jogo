<?php

namespace App\Http\Controllers;

use App\Repositories\ArmaRepository;
use App\Util\Util;
use Illuminate\Http\Request;

use App\Http\Requests;

class ArmaController extends Controller
{

    protected $repository;

    /**
     * ArmaController constructor.
     * @param $repository
     */
    public function __construct(ArmaRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->repository->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
//        dd((is_array($request->all()) ? 'É' : 'NÃO É'));

        $data = $request->all();
        try{
            if (!$this->repository->create($data)) throw new \Exception("Erro ao salvar",500);
        }catch (\Exception $e){
            throw new \Exception($e->getMessage(), $e->getCode());
        }
        return Util::montaRetorno(200, Array("mensagem" => "Arma Salva com Sucesso"));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
