<?php

namespace App\Http\Controllers;

use App\Entities\Personagem;
use App\Repositories\CampoBatalhaRepository;
use App\Services\CampoBatalhaService;
use App\Util\Util;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class CampoBatalhaController extends Controller
{




    /*
     * @var CampoBatalhaService
     */
    protected $service;

    /*
     * @var CampoBata
     */

    protected $repository;

    /**
     * CampoBatalhaController constructor.
     * @param $service
     */
    public function __construct(CampoBatalhaService $service, CampoBatalhaRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->repository->with(["personagem1", "personagem2", "vencedor"])->all();
    }

    //LUTAR
    public function store(Request $request){


        $data = $request->all();

        try {
            DB::beginTransaction();


            if (!is_array($data)) throw new \Exception("Request Nulo", 500);

            if (count($data) == 1) throw new \Exception("Apenas um personagem selecionado", 500);
            $this->service->setAtacante(Personagem::findOrNew($data["personagem1_id"]));
            $this->service->setDefensor(Personagem::findOrNew($data["personagem2_id"]));
            $this->service->lutar();

            DB::commit();

            return Util::montaRetorno(200,array("Mensagem"=> "Luta realizada com sucesso !"));
        }catch (\Exception $e){

            DB::rollback();
            throw new \Exception($e->getMessage(),$e->getCode());
        }





    }
}
