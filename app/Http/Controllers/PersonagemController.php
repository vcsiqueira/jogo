<?php

namespace App\Http\Controllers;

use App\Repositories\PersonagemRepository;
use App\Util\Util;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PersonagemController extends Controller
{

    protected $repository;

    /**
     * PersonagemController constructor.
     * @param $repository
     */
    public function __construct(PersonagemRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->repository->with("arma")->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $data = $request->all();
//        dd($data);
        try{
            if (!$this->repository->create($data)) throw new \Exception("Erro ao salvar",500);
        }catch (\Exception $e){
            throw new \Exception($e->getMessage(), $e->getCode());
        }
        return Util::montaRetorno(200, Array("mensagem" => "Personagem Salva com Sucesso"));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $this->repository->with("arma")->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->repository->update($request->all(),$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
    }
}
