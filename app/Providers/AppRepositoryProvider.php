<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppRepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(
            \App\Repositories\ArmaRepository::class,
            \App\Repositories\ArmaRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\PersonagemRepository::class,
            \App\Repositories\PersonagemRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\CampoBatalhaRepository::class,
            \App\Repositories\CampoBatalhaRepositoryEloquent::class
        );
    }
}
