<?php
/**
 * Created by PhpStorm.
 * User: vcsiqueira
 * Date: 31/07/15
 * Time: 03:27
 */

namespace App\Validators;


use Prettus\Validator\LaravelValidator;

class CampoBatalhaValidator extends LaravelValidator
{
    protected $rules = [
        'personagem1_id' => 'required|integer',
        'personagem2_id' => 'required|integer',
        'vencedor_id' => 'required|integer',
        'ponto_vida_vencedor' => 'required|integer'
    ];
}