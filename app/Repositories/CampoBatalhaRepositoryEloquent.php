<?php
/**
 * Created by PhpStorm.
 * User: vcsiqueira
 * Date: 27/07/15
 * Time: 11:30
 */

namespace App\Repositories;


use App\Entities\CampoBatalha;
use Prettus\Repository\Eloquent\BaseRepository;

class CampoBatalhaRepositoryEloquent extends BaseRepository implements CampoBatalhaRepository
{

    protected $fieldSearchable = ["*"];

    public function model(){
        return CampoBatalha::class;
    }
}