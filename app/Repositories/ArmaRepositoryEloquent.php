<?php
/**
 * Created by PhpStorm.
 * User: vcsiqueira
 * Date: 27/07/15
 * Time: 11:30
 */

namespace App\Repositories;


use App\Entities\Arma;
use Prettus\Repository\Eloquent\BaseRepository;

class ArmaRepositoryEloquent extends BaseRepository implements ArmaRepository
{

    protected $fieldSearchable = ["*"];

    public function model(){
        return Arma::class;
    }
}