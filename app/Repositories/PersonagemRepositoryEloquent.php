<?php
/**
 * Created by PhpStorm.
 * User: vcsiqueira
 * Date: 27/07/15
 * Time: 11:30
 */

namespace App\Repositories;


use App\Entities\Personagem;
use Prettus\Repository\Eloquent\BaseRepository;

class PersonagemRepositoryEloquent extends BaseRepository implements PersonagemRepository
{

    protected $fieldSearchable = ["*"];

    public function model(){
        return Personagem::class;
    }
}