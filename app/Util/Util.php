<?php
/**
 * Created by PhpStorm.
 * User: vcsiqueira
 * Date: 27/10/15
 * Time: 23:58
 */

namespace App\Util;


class Util
{

    public static function montaRetorno($code, $objeto) {
        $retorno = array(
            'response' => array(
                'responsecode' => $code
            ));

        $retorno['response'] = array_merge($retorno['response'], $objeto);

        return $retorno;
    }

}