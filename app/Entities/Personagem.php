<?php
/**
 * Created by PhpStorm.
 * User: vcsiqueira
 * Date: 27/10/15
 * Time: 21:02
 */

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Personagem extends Model
{

    use SoftDeletes;

    public static $tiposPersonagem = [
        'Humano' => 'Humano',
        'Orc' => 'Orc'
    ];

    protected $fillable = [
        'nome',
        'tipo_personagem',
        'ponto_vida',
        'ponto_forca',
        'ponto_agilidade',
        'arma_id'

    ];

    protected $table = "personagem";

    public function arma(){

       return $this->hasOne(Arma::class, "id", "arma_id");
    }


}