<?php
/**
 * Created by PhpStorm.
 * User: vcsiqueira
 * Date: 27/10/15
 * Time: 21:03
 */

namespace App\Entities;



class Dado
{
    public static function getResultado($face){

        if (!is_integer($face)) return 0;
        return rand(1,$face);
    }

}
