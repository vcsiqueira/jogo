<?php
/**
 * Created by PhpStorm.
 * User: vcsiqueira
 * Date: 27/10/15
 * Time: 21:02
 */

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;

class Arma extends Model
{

    protected $table = "arma";

    public static $armas = [
        'Espada Longa' => ["descricao"=>'Espada Longa', "face_dano"=>6],
        'Clava de Madeira' => ["descricao"=>'Clava de Madeira', "face_dano"=>8]
    ];

    protected $fillable = [
        'tipo_arma',
        'ponto_ataque',
        'ponto_defesa',
    ];

    public function getDano(){

        return self::$armas[$this->tipo_arma]["face_dano"];
    }

}