<?php
/**
 * Created by PhpStorm.
 * User: vcsiqueira
 * Date: 27/10/15
 * Time: 21:02
 */

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;

class CampoBatalha extends Model
{

    protected $fillable = [
        'personagem1_id',
        'personagem2_id',
        'vencedor_id',
        'ponto_vida_vencedor'
    ];

    protected $table = "campo_batalha";


    public function personagem1(){
        return $this->hasOne(Personagem::class,"id", "personagem1_id");
    }

    public function personagem2(){
        return $this->hasOne(Personagem::class,"id", "personagem2_id");
    }

    public function vencedor(){
        return $this->hasOne(Personagem::class,"id", "vencedor_id");
    }
}