<?php
/**
 * Created by PhpStorm.
 * User: vcsiqueira
 * Date: 31/07/15
 * Time: 03:25
 */

namespace App\Services;


use App\Entities\Dado;
use App\Entities\Personagem;
use App\Repositories\CampoBatalhaRepository;
use App\Validators\CampoBatalhaValidator;


class CampoBatalhaService
{

    /**
     * @var CampoBatalhaRepository
     */
    protected $repository;

    /**
     * @var CampoBatalhaValidator
     */
    protected $validator;

    /**
     * @var Personagem
     */
    protected $atacante;

    /**
     * @var Personagem
     */
    protected $defensor;


    /**
     * ProjectService constructor.
     */
    public function __construct(CampoBatalhaRepository $repository, CampoBatalhaValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }


    protected function primeiroPasso()
    {

        $agilidadejogador1 = 0;
        $agilidadejogador2 = 0;

        while ($agilidadejogador1 == $agilidadejogador2) {

            //Rola dado de 20 faces
            $agilidadejogador1 = Dado::getResultado(20) + $this->atacante->agilidade;
            $agilidadejogador2 = Dado::getResultado(20) + $this->defensor->agilidade;
            continue;
        }
        $retorno = null;

        if ($agilidadejogador1 > $agilidadejogador2) {
            $retorno["atacante"] = $this->atacante;
            $retorno["defensor"] = $this->defensor;
        }

        if ($agilidadejogador1 < $agilidadejogador2) {
            $retorno["atacante"] = $this->defensor;
            $retorno["defensor"] = $this->atacante;
        }

        $this->atacante = $retorno["atacante"];
        $this->defensor = $retorno["defensor"];

    }

    protected function segundoPasso()
    {
        $this->primeiroPasso();

        $atacante_dado_agilidade_ataqueArma = Dado::getResultado(20) + $this->atacante->ponto_agilidade + $this->atacante->arma()->get()->first()->ponto_ataque;
        $defensor_dado__agilidade_arma_defesa = Dado::getResultado(20) + $this->defensor->ponto_agilidade + $this->defensor->arma()->get()->first()->ponto_defesa;

        if ($atacante_dado_agilidade_ataqueArma > $defensor_dado__agilidade_arma_defesa) {
            $this->getDano();
            return false;
        }

        if ($atacante_dado_agilidade_ataqueArma < $defensor_dado__agilidade_arma_defesa) return false;

        if ($atacante_dado_agilidade_ataqueArma == $defensor_dado__agilidade_arma_defesa) return false;

    }

    protected function getDano()
    {
        $this->defensor->setAttribute("ponto_vida", $this->defensor->ponto_vida - ($this->defensor->arma()->get()->first()->getDano() + $this->defensor->forca));
    }

    public function lutar()
    {
        while ($this->segundoPasso() == false) {
            if ($this->atacante->ponto_vida <= 0) {
                $obj = ['personagem1_id' => $this->defensor->id,
                    'personagem2_id' => $this->atacante->id,
                    'vencedor_id' => $this->defensor->id,
                    'ponto_vida_vencedor' => $this->defensor->ponto_vida];

                $this->repository->create($obj);

                return true;
            }

            if ($this->defensor->ponto_vida <= 0) {
                $obj = ['personagem1_id' => $this->atacante->id,
                    'personagem2_id' => $this->defensor->id,
                    'vencedor_id' => $this->atacante->id,
                    'ponto_vida_vencedor' => $this->atacante->ponto_vida];

                $this->repository->create($obj);
                return true;
            }

        }
    }

    public function setAtacante(Personagem $atacante)
    {
        $this->atacante = $atacante;
    }

    public function setDefensor(Personagem $defensor)
    {
        $this->defensor = $defensor;
    }

}