<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>JOGO</title>
	@if(Config::get("app.debug"))
		<link href="{{asset('build/css/app.css') }}" rel="stylesheet">
        <link href="{{asset('build/css/components.css') }}" rel="stylesheet">
        <link href="{{asset('build/css/flaticon.css') }}" rel="stylesheet">
        <link href="{{asset('build/css/font-awesome.css') }}" rel="stylesheet">
	@else
        <link href="{{elixir('css/all.css')}}" rel="stylesheet">
	@endif

	<!-- Fonts -->
	{{--<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>--}}

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">JOGO</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="{{ url('#/campoBatalha') }}">Batalhas</a></li>
					<li><a href="{{ url('#/personagens') }}">Personagens</a></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">

				</ul>
			</div>
		</div>
	</nav>

	{{--@yield('content')--}}
    <div ng-view></div>

	<!-- Scripts -->
	@if(Config::get("app.debug"))
		<script src="{{ asset('build/js/vendor/jquery.min.js') }}"></script>
		<script src="{{ asset('build/js/vendor/angular.min.js') }}"></script>
		<script src="{{ asset('build/js/vendor/angular-route.min.js') }}"></script>
		<script src="{{ asset('build/js/vendor/angular-resource.min.js') }}"></script>
		<script src="{{ asset('build/js/vendor/angular-animate.min.js') }}"></script>
		<script src="{{ asset('build/js/vendor/angular-messages.min.js') }}"></script>
		<script src="{{ asset('build/js/vendor/ui-bootstrap-tpls.min.js') }}"></script>
		<script src="{{ asset('build/js/vendor/navbar.min.js') }}"></script>



        <script src="{{ asset('build/js/app.js') }}"></script>
        {{--Controllers--}}
        <script src="{{ asset('build/js/controllers/home.js') }}"></script>

        {{--CampoBatalha--}}
        <script src="{{ asset('build/js/controllers/campo-batalha/campoBatalhaList.js') }}"></script>
        <script src="{{ asset('build/js/controllers/campo-batalha/campoBatalhaNew.js') }}"></script>

        {{--Personagem--}}
        <script src="{{ asset('build/js/controllers/personagem/personagemList.js') }}"></script>
        <script src="{{ asset('build/js/controllers/personagem/personagemNew.js') }}"></script>
        <script src="{{ asset('build/js/controllers/personagem/personagemEdit.js') }}"></script>
        <script src="{{ asset('build/js/controllers/personagem/personagemRemove.js') }}"></script>


        {{--Services--}}
        <script src="{{ asset('build/js/services/campoBatalha.js') }}"></script>
        <script src="{{ asset('build/js/services/personagem.js') }}"></script>
        <script src="{{ asset('build/js/services/arma.js') }}"></script>

		{{--Filters--}}
		<script src="{{ asset('build/js/filters/date-br.js') }}"></script>


	@else
        <script src="{{elixir('js/all.js')}}"></script>
	@endif
</body>
</html>
