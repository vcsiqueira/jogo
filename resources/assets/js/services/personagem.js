angular.module('app.services')
.service('Personagem', ['$resource', 'appConfig', function($resource, appConfig){

        function transformData(data) {

            if (angular.isObject(data) && data.hasOwnProperty('cre')){
                var o = angular.copy(data);
                o.due_date = $filter('date')(data.due_date,'yyyy-MM-dd');
                return appConfig.utils.transformRequest(o);
            }

            return data;
        };

        return $resource(appConfig.baseUrl + '/personagem/:id', {id: '@id'},{
            update: {
                method: 'PUT',
                transformRequest: transformData
            },
            //query: {
            //    method: 'GET',
            //    isArray: true,
            //    transformResponse: function(data, header){
            //
            //        var dataJson = JSON.parse(data);
            //
            //        dataJson = dataJson.data;
            //
            //        return dataJson;
            //
            //    }
            //
            //}
        });
    }]);