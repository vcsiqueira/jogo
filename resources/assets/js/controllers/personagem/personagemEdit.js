angular.module('app.controllers')
    .controller('PersonagemEditController',
    ['$scope','$location', '$routeParams','Personagem', 'Arma', 'appConfig',function($scope, $location, $routeParams, Personagem, Arma, appConfig){
        $scope.personagem = Personagem.get({id: $routeParams.id});
        $scope.armas = Arma.query();
        $scope.tipo_personagens = appConfig.personagem.tipo_personagem;
        $scope.selectedItem = $scope.personagem.arma_id;

        $scope.save = function(){
          if ($scope.form.$valid) {
              Personagem.update({id: $scope.personagem.id}, $scope.personagens,function(){
                  $location.path('/personagens');
              });
          }
        };

    }]);/**
 * Created by vcsiqueira on 27/08/15.
 */
