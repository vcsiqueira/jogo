angular.module('app.controllers')
    .controller('CampoBatalhaNewController', ['$scope','$location', 'CampoBatalha', 'Personagem',function($scope, $location, CampoBatalha, Personagem){
        $scope.campoBatalha = new CampoBatalha();
        $scope.personagens = Personagem.query();

        $scope.save = function(){
          if ($scope.form.$valid) {
              $scope.campoBatalha.$save().then(function () {
                  $location.path('/campoBatalha');
              });
          }
        };

    }]);
