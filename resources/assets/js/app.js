var app = angular.module("app",[
    'ngRoute','app.controllers', 'app.services', 'app.filters', 'ui.bootstrap.typeahead', 'ui.bootstrap.tpls'
]);

angular.module('app.controllers',['ngMessages']);
angular.module('app.filters',[]);
angular.module('app.services',['ngResource']);

app.provider('appConfig', ['$httpParamSerializerProvider', function($httpParamSerializerProvider){
   var config = {
       baseUrl: 'http://localhost:8000',
       clientId: 'appid1',
       clientSecret: 'secret',
       personagem: {
           tipo_personagem: [
               {value : "Humano", name: "Humano"},
               {value : "Orc", name: "Orc"}
           ]
       },
       utils: {
           transformRequest: function(data){
               if (angular.isObject(data)){
                    return $httpParamSerializerProvider.$get()(data);
               }
               return data;
           },
           transformResponse: function(data, headers){
               var headersGetter = headers();

               if(headersGetter['content-type'] == 'application/json' ||
                   headersGetter['content-type'] == 'text/json' ){

                   var dataJson = JSON.parse(data);

                   if (dataJson.hasOwnProperty('data')){
                       dataJson = dataJson.data;
                   }

                   return dataJson;
               }
               return data;
           }
       }

   };

    return {
        config: config,
        $get: function(){
            return config;
        }
    }
}]);

app.config([
    '$routeProvider', '$httpProvider', 'appConfigProvider',
    function($routeProvider, $httpProvider, appConfigProvider) {


        //$httpProvider.defaults.transformResponse = appConfigProvider.config.utils.transformResponse;
        //$httpProvider.defaults.transformRequest = appConfigProvider.config.utils.transformRequest;


    $routeProvider

        .when('/campoBatalha', {
            templateUrl: 'build/views/campo-batalha/list.html',
            controller: 'CampoBatalhaListController'
        })

        .when('/campoBatalha/new', {
            templateUrl: 'build/views/campo-batalha/new.html',
            controller: 'CampoBatalhaNewController'
        })

        .when('/personagens',{
            templateUrl: 'build/views/personagem/list.html',
            controller: 'PersonagemListController'
        })

        .when('/personagem/:id/edit',{
            templateUrl: 'build/views/personagem/edit.html',
            controller: 'PersonagemEditController'
        })
        .when('/personagem/:id/remove',{
            templateUrl: 'build/views/personagem/remove.html',
            controller: 'PersonagemRemoveController'
        })
        .when('/personagem/new',{
            templateUrl: 'build/views/personagem/new.html',
            controller: 'PersonagemNewController'
        })

        .when('/home', {
            templateUrl: 'build/views/home.html',
            controller: 'HomeController'
        })
}]);
