angular.module('app.services')
.service('Arma', ['$resource', 'appConfig', function($resource, appConfig){

        function transformData(data) {

            if (angular.isObject(data) && data.hasOwnProperty('cre')){
                var o = angular.copy(data);
                o.due_date = $filter('date')(data.due_date,'yyyy-MM-dd');
                return appConfig.utils.transformRequest(o);
            }

            return data;
        };

        return $resource(appConfig.baseUrl + '/arma/:id', {id: '@id'},{
            update: {
                method: 'PUT',
                transformRequest: transformData
            }
        });
    }]);