angular.module('app.services')
.service('CampoBatalha', ['$resource', 'appConfig', function($resource, appConfig){

        function transformData(data) {

            if (angular.isObject(data) && data.hasOwnProperty('due_date')){
                var o = angular.copy(data);
                o.due_date = $filter('date')(data.due_date,'yyyy-MM-dd');
                return appConfig.utils.transformRequest(o);
            }

            return data;
        };

        return $resource(
            appConfig.baseUrl + '/campoBatalha/:id', {id: '@id'},{
                get:{
                    method: 'GET',
                    transformResponse: function (data, header){
                        var o = appConfig.utils.transformResponse(data, header);
                        if (angular.isObject(o) && o.hasOwnProperty('due_date')){
                            //console.log(o.due_date);
                            var arraDate = o.due_date.split('-');
                            o.due_date = new Date(arraDate[0], parseInt(arraDate[1])-1, arraDate[2]);
                        }

                        return o;
                    }
                }
            });
    }]);