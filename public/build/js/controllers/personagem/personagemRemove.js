angular.module('app.controllers')
    .controller('PersonagemRemoveController',
    ['$scope','$location', '$routeParams','Personagem',function($scope, $location, $routeParams, Personagem){

        $scope.personagem = Personagem.get({id: $routeParams.id});

        $scope.remove = function(){
            $scope.personagem.$delete({id: $scope.personagem.id}).then(function(){
                $location.path('/personagens');
            });
        };

    }]);/**
 * Created by vcsiqueira on 27/08/15.
 */
