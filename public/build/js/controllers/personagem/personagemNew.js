angular.module('app.controllers')
    .controller('PersonagemNewController', ['$scope','$location', 'Personagem','Arma','appConfig',function($scope, $location, Personagem, Arma, appConfig){
        $scope.personagem = new Personagem();
        $scope.armas = Arma.query();
        $scope.tipo_personagens = appConfig.personagem.tipo_personagem;

        $scope.save = function(){
          if ($scope.form.$valid) {
              $scope.personagem.$save().then(function () {
                  $location.path('/personagens');
              });
          }
        };

    }]);/**
 * Created by vcsiqueira on 27/08/15.
 */
