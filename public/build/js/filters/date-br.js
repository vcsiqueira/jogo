/**
 * Created by vcsiqueira on 09/09/15.
 */
angular.module('app.filters').filter('dateBr',['$filter', function($filter){
    return function(input){
        return $filter('date')(input, 'dd/MM/yyyy');
    }
}])