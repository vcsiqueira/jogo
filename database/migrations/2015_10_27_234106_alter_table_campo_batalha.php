<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCampoBatalha extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('campo_batalha', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->foreign('personagem1_id')->references('id')->on('personagem');
            $table->foreign('personagem2_id')->references('id')->on('personagem');
            $table->foreign('vencedor_id')->references('id')->on('personagem');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campo_batalha', function (Blueprint $table) {
            //
        });
    }
}
