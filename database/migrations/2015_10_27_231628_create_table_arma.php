<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableArma extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arma', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo_arma')->unsigned;
            $table->integer('ponto_ataque')->unsigned;
            $table->integer('ponto_defesa')->unsigned;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::drop('arma');
    }
}
