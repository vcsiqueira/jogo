<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePersonagem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('personagem', function (Blueprint $table) {

            $table->engine = 'MyISAM';
            $table->foreign('arma_id')->references('id')->on('arma');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personagem', function (Blueprint $table) {
            //
        });
    }
}
