<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePersonagem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personagem', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome')->unsigned;
            $table->string('tipo_personagem')->unsigned;
            $table->integer('ponto_vida')->unsigned;
            $table->integer('ponto_forca')->unsigned;
            $table->integer('ponto_agilidade')->unsigned;
            $table->integer('arma_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personagem');
    }
}
