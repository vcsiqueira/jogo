<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCampoBatalha extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campo_batalha', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('personagem1_id')->unsigned();
            $table->integer('personagem2_id')->unsigned();
            $table->integer('vencedor_id')->unsigned();
            $table->integer('ponto_vida_vencedor')->unsigned;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campo_batalha');
    }
}
